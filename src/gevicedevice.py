#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
import os
import config
import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceDevice:
  def __init__ (self):
    self.action = "add"

  def add_device (self):
    self.action = "add"
    
    self.entry_name_device.set_sensitive(True)
    self.entry_name_device.set_text(_("NoName"))
    self.entry_ip_device.set_text("127.0.0.1")
    self.entry_comments.set_text("-")
    self.window_device.set_title (_("New device"))

  def modify_device (self,gevice):
    self.action = "mod"
    selected = gevice.get_iter_selected (gevice.gmodel.treeview)
    data = gevice.gmodel.get_data_of_device_from_model(selected)

    self.entry_name_device.set_sensitive(False)
    self.entry_name_device.set_text(data[0])
    self.entry_ip_device.set_text(data[1])
    self.entry_comments.set_text(data[2])
    self.window_device.set_title (_("Modify device"))

  def remove_device (self,gevice):
    self.action = "del"
    selected = gevice.get_iter_selected (gevice.gmodel.treeview)
    data = gevice.gmodel.get_data_of_device_from_model(selected)

    result = gevice.show_message (_("Do you want to remove: ") + data[0],
      Gtk.MessageType.QUESTION,
      (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_YES, Gtk.ResponseType.YES),
      None)

    if result == Gtk.ResponseType.YES:
      i = gevice.get_iter_selected (gevice.gmodel.treeview)
      gevice.gmodel.treestore.remove(i["iter"])

  def load_interface (self,gevice):
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join (config.UIDIR, "device.xml"))

    self.window_device = builder.get_object ("window_device")
    self.entry_name_device = builder.get_object ("entry_name_device")
    self.entry_ip_device = builder.get_object ("entry_ip_device")
    self.entry_comments = builder.get_object ("entry_comments")
    self.button_cancel_device = builder.get_object ("button_cancel_device")
    self.button_accept_device = builder.get_object ("button_accept_device")

    # signals window device
    self.window_device.connect ("delete-event",self.on_window_device_delete_event)
    self.button_cancel_device.connect ("clicked",self.on_button_cancel_device_clicked)
    self.button_accept_device.connect ("clicked",self.on_button_accept_device_clicked,gevice)

  def show_interface (self):
    self.window_device.show_all ()

  def close_window (self,window):
    window.destroy()

  def on_button_cancel_device_clicked (self,button):
    self.close_window (self.window_device)

  def on_window_device_delete_event (self,window,event):
    self.close_window (window)

  def on_button_accept_device_clicked (self,button,gevice):
    alldata = True

    name = self.entry_name_device.get_text()
    ip = self.entry_ip_device.get_text()
    com = self.entry_comments.get_text()

    if (not name.strip()):
      alldata = False
      result = gevice.show_message (_("Enter name of device"),
        None,
        (Gtk.STOCK_OK, Gtk.ResponseType.OK),
        self.window_device)

    if alldata:
      i = gevice.get_iter_selected(gevice.gmodel.treeview)

      if self.action == "add":
        newiter = gevice.gmodel.treestore.append(i["iter"])
      else:
        newiter = i["iter"]

      gevice.gmodel.treestore.set_value (newiter,0,str(name.strip()))
      gevice.gmodel.treestore.set_value (newiter,1,str(ip))
      gevice.gmodel.treestore.set_value (newiter,2,str(com))

      if i["path"]:
        gevice.gmodel.treeview.expand_to_path(i["path"][0])

      self.close_window(self.window_device)
      gevice.actiongroup_model.set_sensitive (True)
