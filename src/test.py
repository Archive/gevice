import gevicedatabase

def process_node(node,line):  
  for ch in node.children:
    print line + ch.name
    process_node(ch,line*2)

def show_data(tree):
  for n in tree.children:
    print n.name
    process_node(n,"--")

dbase = gevicedatabase.GeviceDatabase("../data/other/testing.db")
tree = dbase.get_tree()
show_data(tree)

#data = ("appn6","127.0.0.1")
#dbase.insert_device(data)
#link = "'distn2','appn6'"
#dbase.insert_connections(link)

