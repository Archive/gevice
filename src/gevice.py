#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GdkPixbuf, Vte, GLib

import sys
import os
import os.path

import config
import gevicefind
import gevicediagram
import geviceprefer
import gevicevte
import gevicedevice
import gevicemodel
import gevicedatabase
import geviceexport

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8")
from gettext import gettext as _

class Gevice:
  def __init__ (self, openfilename=None):
    self.dbsqlite = openfilename
    self.connected_to_database = False
    self.context_id = None
    self.session = "ssh"

    self.settings = Gtk.Settings.get_default()
    self.settings.set_property('gtk-application-prefer-dark-theme', True)

    # set treestore model for devices
    self.gmodel = gevicemodel.GeviceModel(self)

    # get widgets of main app
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join (config.UIDIR, "main.xml"))
    
    self.window_main = builder.get_object ("window_main")
    self.window_main.maximize()
    self.vbox_main = builder.get_object ("vbox_main")
    self.hpaned = builder.get_object ("hpaned")
    self.vboxtreeview = builder.get_object ("vboxtreeview")
    self.vboxterminal = builder.get_object ("vboxterminal")
    self.notebook = builder.get_object ("notebook")
    self.statusbar = builder.get_object ("statusbar")
    self.context_id = self.statusbar.get_context_id("status")

    self.window_main.set_icon (GdkPixbuf.Pixbuf.new_from_file(os.path.join (config.ARTDIR,"gevice.png")))
    self.window_main.set_title(config.PACKAGE)
    self.window_main.connect ("delete_event",self.on_delete_event)
    
    # create UIManager instance
    self.uimanager = Gtk.UIManager()
    self.uimanager.add_ui_from_file (os.path.join (config.UIDIR, "menu.xml"))
    
    # create ActionGroup
    self.create_actionsgroups ()
    
    # add the actiongroup to the uimanager
    self.uimanager.insert_action_group (self.actiongroup_window,0)
    self.uimanager.insert_action_group (self.actiongroup_model,0)
    self.uimanager.insert_action_group (self.actiongroup_device,0)
    
    # create MenuBar
    self.menubar = self.uimanager.get_widget ('/MainMenu')
    self.vbox_main.pack_start (self.menubar,False,False,0)
    
    # create a Toolbar and set style
    self.maintoolbar = self.uimanager.get_widget ('/MainToolbar')
    style_context = self.maintoolbar.get_style_context()
    Gtk.StyleContext.add_class (style_context,Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)
    self.vbox_main.pack_start (self.maintoolbar,False,False,0)
    
    self.toolitem_user = Gtk.ToolItem()
    self.entry_user = Gtk.Entry()
    self.entry_user.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, Gtk.STOCK_CLEAR)
    self.entry_user.set_text(_("User"))
    self.entry_user.set_max_length(30)
    self.entry_user.connect ("icon-press", self.icon_press_entry_user);
    self.toolitem_user.add(self.entry_user)
    self.maintoolbar.insert(self.toolitem_user,11)
    
    # infobar
    self.infobar = Gtk.InfoBar()
    self.infobar.add_button (Gtk.STOCK_OK, Gtk.ResponseType.OK)
    self.infobar.connect("response", self.infobar_response_received)    
    self.vbox_main.pack_start(self.infobar,False,False,0)    
    self.label_infobar = Gtk.Label(label=".")
    content = self.infobar.get_content_area()
    content.add(self.label_infobar)
    
    # add treeview to vboxtreeview
    self.scroll = Gtk.ScrolledWindow()
    self.scroll.add (self.gmodel.treeview)
    self.vboxtreeview.add (self.scroll)
    
    # setting notebook properties
    self.notebook.set_scrollable (True)
    self.notebook.set_show_border (True)
    self.notebook.set_show_tabs (True)
    
    self.hpaned.add2 (self.notebook)
    # a negative value means that the position is unset.
    self.hpaned.set_position (250)
    
    # load preferences
    self.gpref = geviceprefer.GevicePrefer(self)
    tcl = self.gmodel.treeview.get_column (1)
    tcl.set_visible (self.gpref.gsettings.get_boolean("viewip"))
    tcl = self.gmodel.treeview.get_column (2)
    tcl.set_visible (self.gpref.gsettings.get_boolean("viewcomments"))
    
    # show all
    self.window_main.show_all()
    self.infobar.hide()

    if self.dbsqlite:
      self.on_action_open(None)
      
    return
  
  def create_actionsgroups (self):
    self.actiongroup_window = Gtk.ActionGroup ('Window Actions')
    self.actiongroup_model = Gtk.ActionGroup ('Model Actions')
    self.actiongroup_device = Gtk.ActionGroup ('Device Actions')
    
    # add the accelerator group to the toplevel window
    self.accelgroup = self.uimanager.get_accel_group()
    self.window_main.add_accel_group (self.accelgroup)

    self.actiongroup_device.add_toggle_actions([
      ('Dual', Gtk.STOCK_NETWORK, '_Dual', None,_('Dual terminal'), None)])

    # creat actions
    self.actiongroup_window.add_actions([
      ('FileMenu', None, _('_File')),
      ('EditMenu', None, _('_Edit')),
      ('ViewMenu', None, _('_View')),
      ('Session', None, _('_Session')),
      ('HelpMenu', None, _('_Help')),
      ('ActionsMenu', None, _('_Actions')),
      ('Quit', Gtk.STOCK_QUIT, _('_Quit'), None, _('Quit the program'), self.on_action_quit),
      ('Prefer', Gtk.STOCK_PREFERENCES, _('Preferences'), None, _('Setting preferences'), self.on_action_preferences),
      ('About', Gtk.STOCK_ABOUT, None, None, None, self.on_action_about),
      ('Nbase', None, _('New Database'),None, _('New Database'), self.on_action_ndbase),
      ('Open', Gtk.STOCK_DISCONNECT, _('Connect'), None, _('Connect to database'), self.on_action_open),
      ])
      
    self.actiongroup_model.add_actions([
      ('Find', Gtk.STOCK_FIND, _('Find'),None, _('Search a device'), self.on_action_find),
      ('Save', Gtk.STOCK_SAVE, _('Save'), None, _('Save model'), self.on_action_save),
      ('Export', Gtk.STOCK_SELECT_ALL, _('Export all to CSV'), None, _('Export all to CSV'), self.on_action_export),
      ('AddDevice', Gtk.STOCK_ADD, _('Add'),None, _('Add a device'), self.on_action_adddevice),
      ])

    self.actiongroup_device.add_actions([
      ('RemoveDevice', Gtk.STOCK_REMOVE, _('Remove'), None, _('Remove device selected'), self.on_action_removedevice),
      ('EditDevice', Gtk.STOCK_EDIT, _('Edit'), None, _('Edit data of device selected'), self.on_action_editdevice),
      ('Expand', Gtk.STOCK_INDENT, _('Expand'), None, _('Expand tree of device selected'), self.on_action_expand),
      ('Decrease', Gtk.STOCK_UNINDENT, _('Decrease'), None, _('Decrease tree of device selected'), self.on_action_decrease),
      ('Diagram', Gtk.STOCK_CONVERT, _('Generate Diagram'),None, _('Generate diagram of devices'), self.on_action_diagram),
      ('Connect', Gtk.STOCK_JUMP_TO, _('Open terminal'), None, _('Open terminal'), self.on_action_connect),
      ])
    
    self.actiongroup_window.add_toggle_actions([
      ('Tree', None, _('Tree'),None, _('Show tree'), self.on_toggle_tree, True),
      ('Terminal', None, _('Terminal'),None, _('Show terminal'), self.on_toggle_terminal, True),
      ('ToolBar', None, _('ToolBar'),None, _('Show/Hide ToolBar'), self.on_toggle_toolbar, True),
      ('DarkTheme', None, _('Dark Theme'),None, _('Show Dark Theme'), self.on_toggle_darktheme, True),
      ])
    
    self.actiongroup_device.add_radio_actions([
      ("ssh", None, _("ssh"), None,  _('SSH Session'), 1),
      ("telnet", None, _("telnet"), None,  _('Telnet Session'), 2)
      ], 1, self.on_action_session)
    
    self.actiongroup_model.set_sensitive (False)
    self.actiongroup_window.set_sensitive (True)
    self.actiongroup_device.set_sensitive (False)

  def main(self):
    Gtk.main()
  
  def show_infobar_message (self,msg,type_msg):
    self.infobar.set_message_type(type_msg)
    self.label_infobar.set_text(msg)    
    self.infobar.show_all()

  def show_message (self,msg,type_msg,buttons,window):
    if (window == None):
      window = self.window_main

    dialog = Gtk.Dialog(config.PACKAGE,window,flags=1,buttons=(buttons))

    label = Gtk.Label(msg)
    content_area = dialog.get_content_area()
    content_area.add (label)
    dialog.show_all()

    result = dialog.run()
    dialog.destroy()

    return result

  def on_action_ndbase(self,action):
    ###FIXME: Open the new database after created
    filechooser = Gtk.FileChooserDialog(title=_("New Database"), 
      parent=self.window_main, 
      action=Gtk.FileChooserAction.SAVE, 
      buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT))

    filechooser.set_select_multiple(False)

    response = filechooser.run()

    if response == Gtk.ResponseType.ACCEPT:
      dbsqlite = filechooser.get_filename()

      try:
        FILE = open (dbsqlite,"w")
        if (FILE):
          FILE.close()
          file_database = os.path.join (config.OTHERDIR,"db_gevice.sql")
          id = os.popen('sqlite3 ' + dbsqlite + ' <' + file_database)          
      except:
        result = self.show_infobar_message (_("Can not write to file"), 
          Gtk.MessageType.ERROR)

    filechooser.destroy()

  def on_delete_event (self,data1=None,data2=None):
    Gtk.main_quit()
  
  def on_action_about (self,action):
    self.create_about_dialog()
    
  def on_action_quit (self,action):
    self.on_delete_event ()

  def get_filename (self):
    filechooser = Gtk.FileChooserDialog(title=_("Open Database"),
      parent=self.window_main, 
      action=Gtk.FileChooserAction.OPEN, 
      buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.ACCEPT))

    filechooser.set_select_multiple(False)
    response = filechooser.run()

    if response == Gtk.ResponseType.ACCEPT:
      dbsqlite = filechooser.get_filename()
      filechooser.destroy()
      return dbsqlite
    else:
      filechooser.destroy()
      return None

  def insert_data_into_model(self,node,iterp):
    for n in node.children:
      itern = self.gmodel.treestore.append(iterp)
      self.gmodel.insert_new_item (itern,n.name,n.ip,n.com)
      self.insert_data_into_model(n,itern)

  def on_action_open (self, action):
    self.gmodel.treestore.clear()
    self.gmodel.treeview.set_model(None)
    
    if (not self.connected_to_database):
      if (self.dbsqlite == None):
        self.dbsqlite = self.get_filename()

      if (self.dbsqlite):
        self.gdbase = gevicedatabase.GeviceDatabase (self.dbsqlite)
        if (self.gdbase.file_is_valid() == False):
          result = self.show_infobar_message (self.gdbase.get_error(), Gtk.MessageType.ERROR)
          self.dbsqlite = None
        elif (self.gdbase.is_connected()):
          tree = self.gdbase.get_tree()
        
          self.gmodel.treeview.set_model(self.gmodel.treestore)
        
          for n in tree.children:
            itern = self.gmodel.treestore.append(None)
            self.gmodel.insert_new_item (itern,n.name,n.ip,n.com)
            self.insert_data_into_model(n, itern)
        
          self.gmodel.treeview.expand_all()
          self.actiongroup_model.set_sensitive(True)
          self.actiongroup_device.set_sensitive (True)
        
          self.update_statusbar (self.dbsqlite)
          self.actiongroup_device.set_sensitive (False)
        
          action_open = self.actiongroup_window.get_action("Open")
          action_open.set_property ("stock-id",Gtk.STOCK_CONNECT)
          action_open.set_property ("tooltip",_("Disconnect from database"))
          action_open.set_property ("label",_("Disconnect"))
          self.connected_to_database = True
        else:
          result = self.show_infobar_message (_("Not connected to database"), Gtk.MessageType.ERROR)
          self.dbsqlite = None
    else:
      action_open = self.actiongroup_window.get_action("Open")
      action_open.set_property ("stock-id",Gtk.STOCK_DISCONNECT)
      action_open.set_property ("tooltip",_("Connect to database"))
      action_open.set_property ("label",_("Connect"))
    
      self.actiongroup_window.set_sensitive (True)
      self.actiongroup_model.set_sensitive (False)
      self.actiongroup_device.set_sensitive (False)
      self.connected_to_database = False
      self.dbsqlite = None
      
      self.update_statusbar("")
    
  def create_about_dialog (self):
    authors = ('Alejandro Valdes Jimenez [avaldes@gnome.org]','')
    ###translator_credits = ('Alejandro Valdes Jimenez [avaldes@gnome.org]')
    logo =  GdkPixbuf.Pixbuf.new_from_file (os.path.join (config.ARTDIR,"gevice.png"))
    
    ad = Gtk.AboutDialog()
    ad.set_name("Gevice - GNOME Network Device Manager")
    ad.set_version(config.VERSION)
    ad.set_authors(authors)
    ###ad.set_translator_credits(translator_credits)
    ad.set_website("http://projects.gnome.org/gevice/")
    ad.set_logo(logo)
    
    ad.run()
    ad.destroy()
    
  def about_delete (self,about,event):
    about.destroy()
    return True
  
  def update_statusbar (self,dbsqlite):
    self.statusbar.push (self.context_id,dbsqlite)

  def infobar_response_received(self,infobar,response):
    if response == Gtk.ResponseType.OK:
      infobar.hide()

  def on_action_adddevice (self,action):
    self.gdevice = gevicedevice.GeviceDevice ()
    self.gdevice.load_interface (self)
    self.gdevice.add_device()
    self.gdevice.show_interface ()

  def on_action_editdevice (self,action):
    self.gdevice = gevicedevice.GeviceDevice ()
    self.gdevice.load_interface (self)
    self.gdevice.modify_device(self)
    self.gdevice.show_interface ()

  def on_action_removedevice (self,action):
    self.gdevice = gevicedevice.GeviceDevice ()
    self.gdevice.remove_device(self)

  def on_action_expand (self,action):
    self.expand_treeview (True)

  def on_action_decrease (self,action):
    self.expand_treeview (False)

  def expand_treeview (self,flag):
    seleccion,iterador = self.gmodel.treeview.get_selection().get_selected()
    path = self.gmodel.treestore.get_path(iterador)

    if flag:
      self.gmodel.treeview.expand_row(path,True)
    else:
      self.gmodel.treeview.collapse_row(path)

  def on_action_save (self, action):
    self.devices_to_connect = []
    self.gdbase.clear_database()

    # save devices into database
    self.gmodel.treestore.foreach(self.copy_device_to_dbase,None)

    if (self.gdbase.get_status_sql()):
      for connected in self.devices_to_connect:
        self.gdbase.insert_connections(connected)    
    
    if (self.gdbase.get_status_sql()):
      result = self.show_infobar_message (_("Model saved"), Gtk.MessageType.INFO)
    else:
      result = self.show_infobar_message (self.gdbase.get_error(), Gtk.MessageType.ERROR)

  def copy_device_to_dbase (self,model,path,iter,data):
    data = self.gmodel.treestore.get(iter,0,1,2)
    
    # save first level.
    self.gdbase.insert_device (data)
    if (self.gdbase.get_status_sql()==False):
      return
    else:
      # If no error, save your children
      if (self.gmodel.treestore.iter_has_child(iter)):
        iterc = self.gmodel.treestore.iter_children(iter)
        next = iterc
      
        while next:
          child = self.gmodel.treestore.get(next,0)
          self.devices_to_connect.append ("'" + data[0] + "','" + child[0] + "'")
          next2 = self.gmodel.treestore.iter_next(next)
          next = next2
    

  def on_toggle_tree (self,action):
    if (action.get_active() == True):
      self.vboxtreeview.show()
    else:
      self.vboxtreeview.hide ()

  def on_toggle_terminal (self,action):
    if (action.get_active() == True):
      self.vboxterminal.show()
    else:
      self.vboxterminal.hide ()

  def on_toggle_toolbar (self,action):
    if (action.get_active() == True):
      self.maintoolbar.show()
    else:
      self.maintoolbar.hide ()
  
  def on_toggle_darktheme (self,action):
    if (action.get_active() == True):
      self.settings.set_property('gtk-application-prefer-dark-theme', True)
    else:
      self.settings.set_property('gtk-application-prefer-dark-theme', False)

  def on_action_session(self,widget,current):
    self.session = current.get_name()

  def on_action_connect(self,action):
    self.connect_to_device()

  def get_iter_selected (self,treeview):
    treeselection = treeview.get_selection()
    model,iter = treeselection.get_selected()
    model,path = treeselection.get_selected_rows()
    selected = {"iter":iter,"path":path,"model":model}
    return selected

  def connect_to_device (self):
    if (self.actiongroup_device.get_action("Dual").get_active()):
      isdual = True
    else:
      isdual = False

    vpaned = Gtk.VPaned()
    vpaned.set_position (-1)
    
    hbox_image = Gtk.HBox(homogeneous=False,spacing=0)
    image = Gtk.Image()
    image.set_from_stock(Gtk.STOCK_CLOSE,Gtk.IconSize.MENU)
    hbox_image.pack_start(image,True,False,False)
    button_close = Gtk.Button()
    button_close.add(hbox_image)
    button_close.set_sensitive (True)
    button_close.connect('clicked',self.close_page_of_notebook,vpaned)
    
    label = Gtk.Label()
    hbox_label = Gtk.HBox(homogeneous=False,spacing=0)
    hbox_label.pack_start(label,False,False,1)
    hbox_label.pack_start(button_close,True,False,0)

    gvte = gevicevte.GeviceVte()

    hbox_term,terminal = gvte.new_terminal (self)
    vpaned.add1 (hbox_term)

    i = self.get_iter_selected (self.gmodel.treeview)
    if not i["iter"]:
      result = gevice.show_infobar_message (_("Not device selected"), Gtk.MessageType.INFO)
      return
    else:
      # get data from device selected (devicename, IP)
      selected = self.get_iter_selected (self.gmodel.treeview)
      data = self.gmodel.get_data_of_device_from_model(selected)
      label.set_text(data[0])

      # get name of user (for ssh session)
      user = self.entry_user.get_text()      

      if (self.session == "ssh"):
        port = 22
        terminal.fork_command_full (Vte.PtyFlags.DEFAULT, None,
          [self.session,'-l',user,'-p',str(port),data[1]],
          [], GLib.SpawnFlags.SEARCH_PATH, None, None)

        if isdual:
          hbox_term2,terminal2 = gvte.new_terminal (self)
          vpaned.add2 (hbox_term2)
          terminal2.fork_command_full (Vte.PtyFlags.DEFAULT, None,
            [self.session,'-l',user,'-p',str(port),data[1]],
            [], GLib.SpawnFlags.SEARCH_PATH, None, None)

      if (self.session == "telnet"):
        port = 23
        terminal.fork_command_full (Vte.PtyFlags.DEFAULT, None,
          [self.session,'-l',user,data[1],str(port)],
          [], GLib.SpawnFlags.SEARCH_PATH, None, None)

        if isdual:
          hbox_term2,terminal2 = gvte.new_terminal (self)
          vpaned.add2 (hbox_term2)
          terminal2.fork_command_full (Vte.PtyFlags.DEFAULT, None,
            [self.session,'-l',user,data[1],str(port)],
            [], GLib.SpawnFlags.SEARCH_PATH, None, None)

    page = self.notebook.append_page(vpaned,hbox_label)
    self.notebook.set_tab_reorderable(vpaned,True)
    self.notebook.set_tab_detachable(vpaned,True)

    hbox_label.show_all()
    vpaned.show_all()
    self.notebook.set_current_page(page)

  def close_page_of_notebook(self,widget,vpaned):
    page = self.notebook.page_num(vpaned)
    self.notebook.remove_page(page)

  def icon_press_entry_user(self, widget, position, event):
    self.entry_user.set_text ("")

  def on_action_preferences (self,action):
    self.gpref.load_interface(self)
    self.gpref.show_interface()

  def on_action_diagram (self,action):
    i = self.get_iter_selected(self.gmodel.treeview)

    #check if the user have selected some device
    if (i["iter"]):
      self.gdiagram = gevicediagram.GeviceDiagram ()
      self.gdiagram.load_interface (self)
      self.gdiagram.show_interface ()
    else:
      result = self.show_message (_("You need choose some device to generate the diagram."),
        None,
        (Gtk.STOCK_OK, Gtk.ResponseType.OK),
        self.window_main)

  def on_action_export (self,action):
    filechooser = Gtk.FileChooserDialog(title=_("Save file as"), 
      parent=self.window_main, 
      action=Gtk.FileChooserAction.SAVE,
      buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT))

    filechooser.set_select_multiple(False)
    response = filechooser.run()

    if response == Gtk.ResponseType.ACCEPT:
      filename = filechooser.get_filename()
      if filename:        
        export = geviceexport.GeviceExport(self.gdbase.get_tree(),
          self.gpref.gsettings.get_string("csvseparator"),filename)
        
        if (export.get_status()):
          result = self.show_infobar_message (_("Model exported"), Gtk.MessageType.INFO)
        else:
          result = self.show_infobar_message (_("Can not write to file"), Gtk.MessageType.ERROR)
          
    filechooser.destroy()

  def on_action_find (self,action):
    self.gevicefind = gevicefind.GeviceFind ()
    self.gevicefind.load_interface(self)
    self.gevicefind.show_interface()

  def on_device_selection_changed (self,selection):
    model,iter = selection.get_selected()

    if (iter):
      self.actiongroup_device.set_sensitive (True)
    else:
      self.actiongroup_device.set_sensitive (False)


if __name__ == "__main__":
  if len(sys.argv)>1:
    # Database to open.
    gevice = Gevice(sys.argv[1])
  else:
    gevice = Gevice()
    
  gevice.main()
