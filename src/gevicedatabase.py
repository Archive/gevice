#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sqlite3
from struct import unpack

import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class Node(object):
  """
  Used to store the devices and create the tree of devices. the attributes are:
  name = name
  ip = ip
  com = comments
  children = []
  """
  def __init__(self, name, ip, com):
    """
    @param name The name of device.
    @param ip The IP address of device.
    @param com The comment.
    """
    self.name = name
    self.ip = ip
    self.com = com
    self.children = []

  def add_child(self, obj):
    """
    Add a node child.
    @param obj The node to add.
    """
    self.children.append(obj)

class GeviceDatabase:
  def __init__ (self,dbsqlite):
    """
    @param dbsqlite The filename of sqlite database.
    """
    # SQLite format 3 (header string)
    self.__magic = ('\x53', '\x51', '\x4c', '\x69', '\x74', '\x65', '\x20', '\x66', '\x6f', '\x72', '\x6d', '\x61', '\x74', '\x20', '\x33', '\x00')
    self.__dbsqlite = dbsqlite
    self.__cur = None
    self.__treedevice = Node("root", None, None)
    self.__valid_file = False
    self.__conn = None
    self.__status_sql = False
    self.__error_string = None
    
    self.__validate_file()
    if self.__valid_file:
      self.__connect()
      if (self.__conn):
        self.__cur = self.__conn.cursor()
          
        if (self.__if_enable_foreign_key() == False):
          self.__enable_foreign_key()
          self.__load_data()

  def get_error(self):
    """
    Return the error occurred.
    """
    return self.__error_string

  def get_status_sql(self):
    """
    Return status of SQL executed.
    """
    return self.__status_sql

  def is_connected(self):
    """
    Return status of connection.
    """
    return self.__conn

  def __validate_file(self):
    """
    Verify valid format of file
    """
    try:
      with open(self.__dbsqlite, 'rb') as handle:
        s = unpack('cccccccccccccccc', handle.read(16))
        if s == self.__magic:
          self.__valid_file = True
    except IOError as e:
      print "I/O error({0}): {1}".format(e.errno, e.strerror)
      self.__error_string = "I/O error({0}): {1}".format(e.errno, e.strerror)

  def get_tree(self):
    """
    Return a tree of devices.
    """
    return self.__treedevice

  def file_is_valid(self):
    """
    Return if file is a valid sqlite3.
    """
    return self.__valid_file

  def __connect (self):
    """
    Connect to database.
    """    
    try:
      self.__conn = sqlite3.connect(self.__dbsqlite)
    except sqlite3.Error as e:
      print "An error occurred:", e.args[0]

  def __enable_foreign_key (self):
    """
    Enable foreign key.
    """
    self.__cur.execute ("PRAGMA foreign_keys = ON ")

  def __if_enable_foreign_key(self):
    """
    Verify if foreign key is enabled.
    """
    self.__cur.execute ("PRAGMA foreign_keys")
    rows = self.__cur.fetchall()

    if (rows[0][0] == 0):
      return False
    else:
      return True

  def __execute_sql_select (self,sql):
    """
    Execute a select SQL.
    @param sql to execute.
    """
    self.__cur.execute(sql)
    rows = self.__cur.fetchall()
    return rows

  def __load_data (self):
    """
    Read and load the data into tree of devices.
    """
    # get root device (or roots)
    sql = "select device.id_dev from device where device.id_dev "
    sql = sql + "not in (select id_devc from connect order by id_devc)"

    rows = self.__execute_sql_select(sql)

    if rows:
      # for each root device, get your children
      token = None
      for row in rows:
        if (not row[0] == token):
          self.__process_row (row[0], self.__treedevice)
          token = row[0]
    
  def __process_row (self,device,rootnode):
    """
    Process each device and your children.
    @param device The name of device to process.
    @param rootnode The root node to which to add the child nodes.
    """
    # get data of device
    sql = "select id_dev, ip_dev, obs_dev from device where id_dev='" + str(device) + "'"

    row = self.__execute_sql_select(sql)
    if row:
      node = Node(row[0][0], row[0][1], row[0][2])
      rootnode.add_child(node)

      # get children of device
      sql2 = "select id_devc from connect where id_devp='" + str(device) + "'"
      rows = self.__execute_sql_select(sql2)

      if rows:
        for children in rows:
          self.__process_row (children[0],node)

  def clear_database(self):
    """
    Clear all data of database.
    """
    self.__execute_sql("delete from connect")
    self.__execute_sql("delete from device")

  def insert_device (self,data):
    """
    Insert data into database.
    @param data The [name,ip,comment] as list.
    """
    sql = "insert into device (id_dev,ip_dev,obs_dev) "
    sql = sql + "values ('" + data[0] + "','" + data[1] + "','" + data[2] + "');"
    self.__execute_sql(sql)

  def insert_connections (self,links):
    """
    Insert links between devices.
    @param links The 'name1','name2' as string
    """
    sql = "insert into connect (id_devp,id_devc) values (" + links + ");"
    self.__execute_sql (sql)

  def __execute_sql (self,sql):
    """
    Execute a SQL.
    """
    self.__status_sql = False
    
    try:
      self.__cur.execute(sql)
      self.__status_sql = True
      self.__conn.commit()
    except sqlite3.Error as e:
      self.__conn.rollback()
      print "An error occurred: ", e.args[0]
      self.__error_string = e.args[0]
