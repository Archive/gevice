#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Vte, Gdk
import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceVte:
  def __init__ (self):
    pass

  def the_focus (self,widget,event,terminal):
    if (event.type==Gdk.EventType.ENTER_NOTIFY):
      terminal.grab_focus()

  def new_terminal (self,gevice):       
    hbox_term = Gtk.HBox(homogeneous=False,spacing=0)

    terminal = Vte.Terminal()
    terminal.set_mouse_autohide(True)       
    terminal.set_default_colors()
    
    bgcolor = Gdk.color_parse(gevice.gpref.gsettings.get_string("backcolor"))
    terminal.set_color_background(bgcolor)
    
    fgcolor = Gdk.color_parse(gevice.gpref.gsettings.get_string("forecolor"))
    terminal.set_color_foreground(fgcolor)
    
    terminal.set_size_request(10,10)
    terminal.set_size (30,1)
    terminal.set_mouse_autohide (True)    

    # capture word limited for characters when double click
    terminal.set_word_chars("-A-Za-z0-9,./?%&#:_")

    terminal.connect('event',self.the_focus,terminal)

    adj = terminal.get_vadjustment()
    scroll = Gtk.VScrollbar.new(adj)

    hbox_term.pack_start(terminal,True,True,0)
    hbox_term.pack_start(scroll,False,False,0)

    return hbox_term,terminal
