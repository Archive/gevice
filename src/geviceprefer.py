#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk, Gio
import os

import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8")
from gettext import gettext as _

class GevicePrefer:
  def __init__ (self,gevice):
    self.gsettings = Gio.Settings.new("apps.gevice")

  def load_interface (self,gevice):
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join (config.UIDIR, "pref.xml"))

    self.window_prefer = builder.get_object ("window_prefer")
    self.button_close_prefer = builder.get_object ("button_close_prefer")
    self.checkbutton_ip = builder.get_object ("checkbutton_ip")
    self.checkbutton_com = builder.get_object ("checkbutton_com")
    self.entry_backcolor = builder.get_object ("entry_backcolor")
    self.entry_forecolor = builder.get_object ("entry_forecolor")
    self.entry_csvseparator = builder.get_object ("entry_csvseparator")
    self.button_backcolor = builder.get_object ("button_backcolor")
    self.button_forecolor = builder.get_object ("button_forecolor")
    self.checkbutton_ip_diag = builder.get_object ("checkbutton_ip_diag")

    # signals
    self.window_prefer.connect ("delete-event", self.on_window_prefer_delete_event)
    self.button_close_prefer.connect ("clicked", self.on_button_close_prefer_clicked)
    
    self.checkbutton_ip.connect ("toggled", self.show_column, 1, gevice, "viewip")
    self.checkbutton_com.connect ("toggled", self.show_column, 2, gevice, "viewcomments")
    
    self.entry_backcolor.connect ("changed", self.on_entry_backcolor_changed, gevice, "backcolor")
    self.entry_forecolor.connect ("changed", self.on_entry_forecolor_changed, gevice, "forecolor")
    self.entry_csvseparator.connect ("changed", self.on_entry_csvseparator_changed, gevice, "csvseparator")
    
    self.checkbutton_ip_diag.connect ("toggled", self.change_estatus_on_diagram, gevice, "diagip")
    
    self.button_backcolor.connect ("clicked", self.on_button_background_clicked, self.entry_backcolor)
    self.button_forecolor.connect ("clicked", self.on_button_foreground_clicked, self.entry_forecolor)
  
      # setting preferences
    self.checkbutton_ip.set_active(self.gsettings.get_boolean("viewip"))
    self.checkbutton_com.set_active(self.gsettings.get_boolean("viewcomments"))
   
    self.entry_backcolor.set_text (self.gsettings.get_string("backcolor"))
    self.entry_forecolor.set_text (self.gsettings.get_string("forecolor"))
    self.entry_csvseparator.set_text (self.gsettings.get_string("csvseparator"))
    
    self.checkbutton_ip_diag.set_active(self.gsettings.get_boolean("diagip"))

  def show_interface (self):
    self.window_prefer.show_all()

  def close_window (self,window):
    window.destroy()

  def on_button_background_clicked (self,button,entry):
    self.create_color_selection_dialog(_("Select background color"),entry)

  def on_button_foreground_clicked (self,button,entry):
    self.create_color_selection_dialog(_("Select text color"),entry)

  def create_color_selection_dialog (self,title,entry):
    # current color
    colorstr = entry.get_text()
    colorgdk = Gdk.color_parse(colorstr)

    colord = Gtk.ColorSelectionDialog(title)
    colorselection = colord.get_color_selection()
    colorselection.set_current_color(colorgdk)

    response = colord.run()

    if response == Gtk.ResponseType.OK:
      gdkcolor = colorselection.get_current_color()
      color = gdkcolor.to_string()
      entry.set_text(color)

    colord.destroy()

  def on_button_close_prefer_clicked (self,button):
    self.close_window (self.window_prefer)

  def on_window_prefer_delete_event (self,window,event):
    self.close_window (window)

  def on_entry_backcolor_changed (self,editable,gevice,attr):
    self.gsettings.set_string(attr, editable.get_text ())

  def on_entry_csvseparator_changed (self,editable,gevice,attr):
    self.gsettings.set_string(attr, editable.get_text ())

  def on_entry_forecolor_changed (self,editable,gevice,attr):
    self.gsettings.set_string(attr, editable.get_text ())

  def show_column (self, checkbutton, column, gevice, attr):
    # update preference
    self.gsettings.set_boolean(attr, checkbutton.get_active())
    
    st =  checkbutton.get_active()
    tcl = gevice.gmodel.treeview.get_column (column)

    if (st):
      tcl.set_visible (True)
    else:
      tcl.set_visible (False)

  def change_estatus_on_diagram (self,checkbutton,gevice, attr):
    self.gsettings.set_boolean(attr, checkbutton.get_active())
